﻿using System.ComponentModel.DataAnnotations;

namespace CashRegisterRepairs.Model
{
    //TODO: Test service profile validations...
    public class ServiceProfile
    {
        public const int NAME_IDX = 0;
        public const int BULSTAT_IDX = 1;
        public const int ADDRESS_IDX = 2;
        public const int MANAGER_IDX = 3;
        public const int PHONE_IDX = 4;
        public const int LATITUDE_IDX = 5;
        public const int LONGTITUDE_IDX = 6;

        public const char IDX_SEPARATOR = ',';

        [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за име на сервиз не е попълнено!")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за булстат на сервиз не е попълнено!")]
        [StringLength(9, MinimumLength = 9, ErrorMessage = "Въведения булстат не съвпада с точно указаната дължина: 9 !")]
        [RegularExpression(@"\d\d\d\d\d\d\d\d\d", ErrorMessage = "Въведения булстат не следва правилния формат: само цифри, дължина - 9 !")]
        public string Bulstat { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за адрес на сервиз не е попълнено!")]
        public string Address { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за управител на сервиз не е попълнено!")]
        [StringLength(50, ErrorMessage = "Въведените имена надвишават позволената максимална дължина: 50 !")]
        [RegularExpression(@"(\w+\s\w+)+|(\w+)", ErrorMessage = "Въведените имена на управител не следва правилния формат: само дума/и(разделени с интервал) !")]
        public string Manager { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за телефон на управител не е попълнено!")]
        [StringLength(20, ErrorMessage = "Въведения телефон надвишава позволената максимална дължина: 20 !")]
        [Phone(ErrorMessage = "Въведения телефон на управител не следва правилния формат: цифри и символи, без букви !")]
        public string Phone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Няма избрани координати на сервиз!")]
        [RegularExpression(@"\d+\.\d+", ErrorMessage = "Избраните координати на сервиза са невалидни!")]
        public string Latitude { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Няма избрани координати на сервиз!")]
        [RegularExpression(@"\d+\.\d+", ErrorMessage = "Избраните координати на сервиза са невалидни!")]
        public string Longtitude { get; set; }

        public static bool isCompleteProfile(string[] parsedProfile)
        {
            return (parsedProfile.Length == typeof(ServiceProfile).GetProperties().Length);
        }
    }
}
