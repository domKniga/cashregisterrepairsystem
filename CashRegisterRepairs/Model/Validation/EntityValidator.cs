﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CashRegisterRepairs.Model.Validation
{
    public sealed class EntityValidator
    {
        /// See http://odetocode.com/Blogs/scott/archive/2011/06/29/manual-validation-with-data-annotations.aspx
        public static bool TryValidateEntity(object @object, out ICollection<ValidationResult> results, bool validateOnlyRequiredProperties = true)
        {
            var context = new ValidationContext(@object, serviceProvider: null, items: null);
            results = new List<ValidationResult>();
            return Validator.TryValidateObject(
                @object, context, results,
                validateAllProperties: (!validateOnlyRequiredProperties)
            );
        }
    }
}