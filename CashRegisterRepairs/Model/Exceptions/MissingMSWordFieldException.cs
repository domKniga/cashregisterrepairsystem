﻿using System;

namespace CashRegisterRepairs.Model.Exceptions
{
    public class MissingMSWordFieldException : Exception
    {
        public MissingMSWordFieldException(string field, string xmlField, string xPath)
            : base("Не е намерен елемент: [" + field + "] в MS Word шаблона за XML елемент: [" + xmlField + "] с път [" + xPath + "]!")
        {
        }

        public MissingMSWordFieldException(string field, string xmlField, string xPath, Exception innerException)
            : base("Не е намерен елемент: [" + field + "] в MS Word шаблона за XML елемент: [" + xmlField + "] с път [" + xPath + "]!")
        {
        }
    }
}
