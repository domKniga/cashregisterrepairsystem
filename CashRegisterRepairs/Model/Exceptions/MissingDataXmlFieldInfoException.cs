﻿using System;

namespace CashRegisterRepairs.Model.Exceptions
{
    public class MissingDataXmlFieldInfoException : Exception
    {
        public MissingDataXmlFieldInfoException(string xmlNode, string templateType)
            : base("Не е намеренa информация за XML елемент: [" + xmlNode + "] в шаблон: " + templateType)
        {
        }

        public MissingDataXmlFieldInfoException(string xmlNode, string templateType, Exception innerException)
            : base("Не е намеренa информация за XML елемент: [" + xmlNode + "] в шаблон: " + templateType, innerException)
        {
        }
    }
}
