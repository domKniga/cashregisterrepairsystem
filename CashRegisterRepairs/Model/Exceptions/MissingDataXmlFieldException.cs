﻿using System;

namespace CashRegisterRepairs.Model.Exceptions
{
    public class MissingDataXmlFieldException : Exception
    {
        public MissingDataXmlFieldException(string xmlField, string xPath)
            : base("Не е намерен елемент: [" + xmlField + "] с път: [" + xPath + "] в XML шаблона!")
        {
        }

        public MissingDataXmlFieldException(string xmlField, string xPath, Exception innerException)
            : base("Не е намерен елемент: [" + xmlField + "] с път: [" + xPath + "] в XML шаблона!", innerException)
        {
        }
    }
}
