﻿using System;

namespace CashRegisterRepairs.Model.Exceptions
{
    public class MissingServiceProfileInfoException : Exception
    {
        public MissingServiceProfileInfoException(string message) : base(message)
        {
        }

        public MissingServiceProfileInfoException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
