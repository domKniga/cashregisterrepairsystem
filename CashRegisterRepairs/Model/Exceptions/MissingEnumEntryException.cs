﻿using System;

namespace CashRegisterRepairs.Model.Exceptions
{
    public class MissingEnumEntryException : ArgumentException
    {
        public MissingEnumEntryException(string findByValue, string enumName)
            : base("Няма стойност отговаряща на: " + findByValue, enumName)
        {
        }

        public MissingEnumEntryException(string findByValue, string enumName, Exception innerException)
            : base("Няма стойност отговаряща на: " + findByValue, enumName, innerException)
        {
        }
    }
}
