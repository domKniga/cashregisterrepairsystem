﻿using System;

namespace CashRegisterRepairs.Model.Exceptions
{
    public class MissingViewException : Exception
    {
        public MissingViewException(string viewType)
            : base("Няма отворен прозорец от тип: " + viewType)
        {
        }

        public MissingViewException(string viewType, Exception innerException)
            : base("Няма отворен прозорец от тип: " + viewType, innerException)
        {
        }
    }
}
