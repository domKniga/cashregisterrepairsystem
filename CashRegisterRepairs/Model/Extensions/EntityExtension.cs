﻿using CashRegisterRepairs.Model.Exceptions;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using static CashRegisterRepairs.Model.Extensions.EntityExtension.Template;

namespace CashRegisterRepairs.Model.Extensions
{
    public class EntityExtension
    {
        [MetadataType(typeof(EntityMetadata.ClientMetadata))]
        public partial class Client
        {
        }

        [MetadataType(typeof(EntityMetadata.ManagerMetadata))]
        public partial class Manager
        {
        }

        [MetadataType(typeof(EntityMetadata.SiteMetadata))]
        public partial class Site
        {
        }

        [MetadataType(typeof(EntityMetadata.DeviceModelMetadata))]
        public partial class DeviceModel
        {
        }

        [MetadataType(typeof(EntityMetadata.DeviceMetadata))]
        public partial class Device
        {
        }

        public partial class Template
        {
            public enum TemplateType
            {
                [Description("Договор")]
                Contract,
                [Description("Свидетелство")]
                Certificate,
                [Description("Протокол")]
                Protocol
            }

            public enum TemplateStatus
            {
                [Description("ЗАДЪЛЖИТЕЛЕН")]
                Required,
                [Description("НЕЗАДЪЛЖИТЕЛЕН")]
                NotRequired
            }
        }
    }

    //TODO: Create unit tests - mark public method + rclick + create methods(download NUnit first)...
    public static class TemplateExtensions
    {
        public static TemplateType GetType(string displayName)
        {
            Array values = Enum.GetValues(typeof(TemplateType));
            foreach (TemplateType val in values)
            {
                if (string.Equals(val.ToDisplayName(), displayName))
                {
                    return val;
                }
            }

            throw new MissingEnumEntryException(displayName, typeof(TemplateType).ToString());
        }

        public static TemplateStatus GetStatus(string displayName)
        {
            Array values = Enum.GetValues(typeof(TemplateStatus));
            foreach (TemplateStatus val in values)
            {
                if (string.Equals(val.ToDisplayName(), displayName))
                {
                    return val;
                }
            }

            throw new MissingEnumEntryException(displayName, typeof(TemplateStatus).ToString());
        }

        public static string ToDisplayName(this TemplateType val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType()
                .GetField(val.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false);

            return (attributes.Length > 0) ? attributes[0].Description : string.Empty;
        }

        public static string ToDisplayName(this TemplateStatus val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType()
                .GetField(val.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false);

            return (attributes.Length > 0) ? attributes[0].Description : string.Empty;
        }
    }
}
