﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CashRegisterRepairs.Model.Extensions
{
    /// See https://docs.microsoft.com/en-us/aspnet/mvc/overview/getting-started/database-first-development/enhancing-data-validation
    public class EntityMetadata
    {
        //TODO: Test entity validations...
        public class ClientMetadata
        {
            public int ID { get; set; }

            [StringLength(10, MinimumLength = 10, ErrorMessage = "Въведеното ЕГН не съвпада с точно указаната дължина: 10 !")]
            [RegularExpression(@"\d\d\d\d\d\d\d\d\d\d", ErrorMessage = "Въведеното ЕГН не следва правилния формат: само цифри, дължина - 10 !")]
            public string EGN { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за име на клиент не е попълнено!")]
            [StringLength(50, ErrorMessage = "Въведените имена надвишават позволената максимална дължина: 50 !")]
            [RegularExpression(@"(\w+\s\w+)+|(\w+)", ErrorMessage = "Client name must follow the correct name format: [name][whitespace][name] !")]
            public string NAME { get; set; }

            [StringLength(9, MinimumLength = 9, ErrorMessage = "Въведения булстат не съвпада с точно указаната дължина: 9 !")]
            [RegularExpression(@"\d\d\d\d\d\d\d\d\d", ErrorMessage = "Въведения булстат не следва правилния формат: само цифри, дължина - 9 !")]
            public string BULSTAT { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за адрес на клиент не е попълнено!")]
            public string ADDRESS { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за ТДД на клиент не е попълнено!")]
            public string TDD { get; set; }

            public string COMMENT { get; set; }
        }

        public class ManagerMetadata
        {
            public int ID { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за име на управител не е попълнено!")]
            [StringLength(50, ErrorMessage = "Въведените имена надвишават позволената максимална дължина: 50 !")]
            [RegularExpression(@"(\w+\s\w+)+|(\w+)", ErrorMessage = "Въведените имена на управител не следва правилния формат: само дума/и(разделени с интервал) !")]
            public string NAME { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за телефон на управител не е попълнено!")]
            [StringLength(20, ErrorMessage = "Въведения телефон надвишава позволената максимална дължина: 20 !")]
            [Phone(ErrorMessage = "Въведения телефон на управител не следва правилния формат: цифри и символи, без букви !")]
            public string PHONE { get; set; }
        }

        public class SiteMetadata
        {
            public int ID { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за име на обект не е попълнено!")]
            [StringLength(50, ErrorMessage = "Въведенoтo име надвишават позволената максимална дължина: 50 !")]
            [RegularExpression(@"(\w+\s\w+)+|(\w+)", ErrorMessage = "Въведените имена на управител не следва правилния формат: само дума/и(разделени с интервал) !")]
            public string NAME { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за адрес на обект не е попълнено!")]
            [StringLength(20, ErrorMessage = "Въведения адрес надвишава позволената максимална дължина: 20 !")]
            public string ADDRESS { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за телефон на обект не е попълнено!")]
            [StringLength(20, ErrorMessage = "Въведения телефон надвишава позволената максимална дължина: 20 !")]
            [Phone(ErrorMessage = "Въведения телефон на обект не следва правилния формат: цифри и символи, без букви !")]
            public string PHONE { get; set; }
        }

        public class DeviceModelMetadata
        {
            public int ID { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за производител на модел не е попълнено!")]
            [StringLength(20, ErrorMessage = "Въведения производител надвишава позволената максимална дължина: 20 !")]
            public string MANUFACTURER { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за модел не е попълнено!")]
            [StringLength(20, ErrorMessage = "Въведения модел надвишава позволената максимална дължина: 20 !")]
            public string MODEL { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за сертификат на модел не е попълнено!")]
            [StringLength(20, ErrorMessage = "Въведения сертификат надвишава позволената максимална дължина: 20 !")]
            [RegularExpression(@"\d\d\d\/[0-3][0-9]\.[0-1][0-9]\.[0-9][0-9][0-9][0-9]", ErrorMessage = "Въведените имена на управител не следва правилния формат: [###][.][dd.MM.yyyy] !")]
            public string CERTIFICATE { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за префикс номер на апарат не е попълнено!")]
            [StringLength(2, MinimumLength = 2, ErrorMessage = "Въведения префикс номер не съвпада с точно указаната дължина: 2 !")]
            [RegularExpression(@"[a-zA-Z][a-zA-Z]", ErrorMessage = "Въведения префикс номер не следва правилния формат: само букви, дължина - 2 !")]
            public string DEVICE_NUM_PREFIX { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за префикс фискален номер на апарат не е попълнено!")]
            [StringLength(2, MinimumLength = 2, ErrorMessage = "Въведения префикс номер не съвпада с точно указаната дължина: 2 !")]
            [RegularExpression(@"[a-zA-Z][a-zA-Z]", ErrorMessage = "Въведения префикс номер не следва правилния формат: само букви, дължина - 2 !")]
            public string FISCAL_NUM_PREFIX { get; set; }
        }

        public class DeviceMetadata
        {
            public int ID { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за SIM на апарат не е попълнено!")]
            [StringLength(15, MinimumLength = 15, ErrorMessage = "Въведения SIM не съвпада с точно указаната дължина: 15 !")]
            [RegularExpression(@"\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d", ErrorMessage = "Въведения SIM не следва правилния формат: само цифри, дължина - 15 !")]
            public string SIM { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за постфикс номер на апарат не е попълнено!")]
            [StringLength(6, MinimumLength = 6, ErrorMessage = "Въведения постфикс номер не съвпада с точно указаната дължина: 6 !")]
            [RegularExpression(@"\d\d\d\d\d\d", ErrorMessage = "Въведения постфикс номер не следва правилния формат: само цифри, дължина - 6 !")]
            public string DEVICE_NUM_POSTFIX { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за постфикс фискален номер на апарат не е попълнено!")]
            [StringLength(6, MinimumLength = 6, ErrorMessage = "Въведения постфикс номер не съвпада с точно указаната дължина: 6 !")]
            [RegularExpression(@"\d\d\d\d\d\d", ErrorMessage = "Въведения постфикс номер не следва правилния формат: само цифри, дължина - 6 !")]
            public string FISCAL_NUM_POSTFIX { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за НАП номер на апарат не е попълнено!")]
            [StringLength(7, MinimumLength = 7, ErrorMessage = "Въведения НАП номер не съвпада с точно указаната дължина: 7 !")]
            [RegularExpression(@"\d\d\d\d\d\d\d", ErrorMessage = "Въведения НАП номер не следва правилния формат: само цифри, дължина - 7 !")]
            public string NAP_NUMBER { get; set; }

            [Required(AllowEmptyStrings = false, ErrorMessage = "Полето за НАП дата на апарат не е попълнено!")]
            public System.DateTime NAP_DATE { get; set; }
        }

        public static void RegisterMetadataTypes()
        {
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Manager), typeof(ManagerMetadata)), typeof(Manager));
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Client), typeof(ClientMetadata)), typeof(Client));
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Site), typeof(SiteMetadata)), typeof(Site));
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(DeviceModel), typeof(DeviceModelMetadata)), typeof(DeviceModel));
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Device), typeof(DeviceMetadata)), typeof(Device));
        }
    }
}
