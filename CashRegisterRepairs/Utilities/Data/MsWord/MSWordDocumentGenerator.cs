﻿using CashRegisterRepairs.Model.Exceptions;
using CashRegisterRepairs.Model.Extensions;
using CashRegisterRepairs.Utilities.Data.Xml;
using CashRegisterRepairs.Utilities.Locators;
using Microsoft.Office.Interop.Word;
using System;
using System.Xml;
using static CashRegisterRepairs.Model.Extensions.EntityExtension.Template;

namespace CashRegisterRepairs.Utilities.Data.MsWord
{
    public sealed class MSWordDocumentGenerator
    {
        public static void BuildWordDocument(Model.Document document, Model.Template template)
        {
            XmlDocument dataXml = new XmlDocument();
            dataXml.LoadXml(document.DOC);

            Application wordApp = new Application();
            Document wordDoc = new Document();

            string templateTypeDisplayName = template.TYPE;
            object oMissing = System.Reflection.Missing.Value;
            object oTemplatePath = ResourceLocator.FetchWordTemplatePath(templateTypeDisplayName);
            wordDoc = wordApp.Documents.Add(ref oTemplatePath, ref oMissing, ref oMissing, ref oMissing);

            foreach (Field myMergeField in wordDoc.Fields)
            {
                Range rngFieldCode = myMergeField.Code;

                string fieldText = rngFieldCode.Text;

                if (fieldText.StartsWith(" MERGEFIELD"))
                {
                    int endMerge = fieldText.IndexOf("\\");
                    int fieldNameLength = fieldText.Length - endMerge;
                    string fieldName = fieldText.Substring(11, endMerge - 11).Trim();

                    myMergeField.Select();

                    TemplateType templateType = TemplateExtensions.GetType(templateTypeDisplayName);
                    XmlDataNodeInfo dataNode = XmlDataNodeInfo.FindNode(templateType, fieldName);

                    try
                    {
                        wordApp.Selection.TypeText(dataXml.SelectSingleNode(dataNode.XPath).InnerText);
                    }
                    catch (NullReferenceException nre)
                    {
                        // Correct way to close word app - first close doc without saving changes...
                        wordDoc.Close(false);
                        wordApp.Quit();

                        throw new MissingMSWordFieldException(fieldName, dataNode.NodeId, dataNode.XPath, nre);
                    }
                }
            }

            string tempFilePath = ResourceLocator.FetchTemporaryWordDocumentFullPath();
            wordDoc.SaveAs(tempFilePath);
            wordApp.Documents.Open(tempFilePath);
        }
    }
}
