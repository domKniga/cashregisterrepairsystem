﻿using CashRegisterRepairs.Model;
using System.Collections.Generic;
using System.Linq;
using static CashRegisterRepairs.Model.Extensions.EntityExtension.Template;
using static CashRegisterRepairs.Model.Extensions.TemplateExtensions;

namespace CashRegisterRepairs.Utilities.Data.MsWord
{
    public sealed class DocumentWatchdog
    {
        private static bool isContractRequired;
        private static bool isCertificateRequired;
        private static bool isProtocolRequired;

        public static void DetermineRequiredDocuments()
        {
            using (CashRegisterServiceContext dbModel = new CashRegisterServiceContext())
            {
                // Linq queries don`t support extension methods - see more: https://msdn.microsoft.com/en-us/library/bb738550(v=vs.110).aspx
                // Workaround -> convert data collection to Enumerable first.
                isContractRequired = dbModel.Templates.AsEnumerable().Any(t => t.TYPE.Equals(TemplateType.Contract.ToDisplayName()) && t.STATUS == TemplateStatus.Required.ToDisplayName());
                isCertificateRequired = dbModel.Templates.AsEnumerable().Any(t => t.TYPE.Equals(TemplateType.Certificate.ToDisplayName()) && t.STATUS == TemplateStatus.Required.ToDisplayName());
                isProtocolRequired = dbModel.Templates.AsEnumerable().Any(t => t.TYPE.Equals(TemplateType.Protocol.ToDisplayName()) && t.STATUS == TemplateStatus.Required.ToDisplayName());
            }
        }

        public static bool ValidateRequiredDocuments(Device device)
        {
            IEnumerable<Document> documentsForDevice = Enumerable.Empty<Document>();

            bool hasContract;
            bool hasCertificate;
            bool hasProtocol;

            using (CashRegisterServiceContext dbModel = new CashRegisterServiceContext())
            {
                // Convert dataset to an Enumerable, see above method for explanation.
                documentsForDevice = dbModel.Documents.Where(doc => doc.DEVICE_ID == device.ID).AsEnumerable();

                hasContract = (documentsForDevice.Where(doc => doc.Template.TYPE.Equals(TemplateType.Contract.ToDisplayName())).Count() != 0);
                hasCertificate = (documentsForDevice.Where(doc => doc.DEVICE_ID == device.ID && doc.Template.TYPE.Equals(TemplateType.Certificate.ToDisplayName())).Count() != 0);
                hasProtocol = (documentsForDevice.Where(doc => doc.DEVICE_ID == device.ID && doc.Template.TYPE.Equals(TemplateType.Protocol.ToDisplayName())).Count() != 0);
            }

            return (isRequirementMet(isContractRequired, hasContract) && isRequirementMet(isCertificateRequired, hasCertificate) && isRequirementMet(isProtocolRequired, hasProtocol));
        }

        public static bool isRequirementMet(bool requiresDocument, bool hasDocument)
        {
            if (!requiresDocument)
            {
                return true;
            }

            return requiresDocument.Equals(hasDocument);
        }
    }
}
