﻿using CashRegisterRepairs.Model.Exceptions;
using System;
using System.Reflection;
using static CashRegisterRepairs.Model.Extensions.EntityExtension.Template;

namespace CashRegisterRepairs.Utilities.Data.Xml
{
    public sealed class XmlDataNodeInfo
    {
        public string NodeId { get; }
        public string XPath { get; }

        private XmlDataNodeInfo(string nodeId, string xPath)
        {
            NodeId = nodeId;
            XPath = xPath;
        }

        public static XmlDataNodeInfo FindNode(TemplateType type, string nodeId)
        {
            FieldInfo[] dataNodeConstants = typeof(XmlDataNodeInfo).GetFields(BindingFlags.Public | BindingFlags.Static);
            foreach (FieldInfo dataNodeConstant in dataNodeConstants)
            {
                // If the info constant is not prepended by the appropriate template type name - skip it.
                if (!dataNodeConstant.Name.StartsWith(type.ToString(), StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                XmlDataNodeInfo dataNodeInfo = (dataNodeConstant.GetValue(null) as XmlDataNodeInfo);
                if (string.Equals(dataNodeInfo.NodeId, nodeId))
                {
                    return dataNodeInfo;
                }
            }

            throw new MissingDataXmlFieldInfoException(nodeId, type.GetType().ToString());
        }

        // Contract nodes
        public static readonly XmlDataNodeInfo CONTRACT_META_CURRENT_DATE = new XmlDataNodeInfo("MetaCurrentDate", "ContractTemplate/Metadata/MetaCurrentDate");
        public static readonly XmlDataNodeInfo CONTRACT_META_REF_NUMBER = new XmlDataNodeInfo("MetaRefNumber", "ContractTemplate/Metadata/MetaRefNumber");
        public static readonly XmlDataNodeInfo CONTRACT_META_START_DATE = new XmlDataNodeInfo("MetaStartDate", "ContractTemplate/Metadata/MetaStartDate");
        public static readonly XmlDataNodeInfo CONTRACT_META_END_DATE = new XmlDataNodeInfo("MetaEndDate", "ContractTemplate/Metadata/MetaEndDate");
        public static readonly XmlDataNodeInfo CONTRACT_META_SUPPORT_FEE = new XmlDataNodeInfo("MetaSupportFee", "ContractTemplate/Metadata/MetaSupportFee");

        public static readonly XmlDataNodeInfo CONTRACT_CLIENT_NAME = new XmlDataNodeInfo("ClientName", "ContractTemplate/Client/ClientName");
        public static readonly XmlDataNodeInfo CONTRACT_CLIENT_BULSTAT = new XmlDataNodeInfo("ClientBulstat", "ContractTemplate/Client/ClientBulstat");
        public static readonly XmlDataNodeInfo CONTRACT_CLIENT_ADDRESS = new XmlDataNodeInfo("ClientAddress", "ContractTemplate/Client/ClientAddress");
        public static readonly XmlDataNodeInfo CONTRACT_CLIENT_MANAGER = new XmlDataNodeInfo("ClientManager", "ContractTemplate/Client/ClientManager");

        public static readonly XmlDataNodeInfo CONTRACT_SERVICE_NAME = new XmlDataNodeInfo("ServiceName", "ContractTemplate/Service/ServiceName");
        public static readonly XmlDataNodeInfo CONTRACT_SERVICE_ADDRESS = new XmlDataNodeInfo("ServiceAddress", "ContractTemplate/Service/ServiceAddress");
        public static readonly XmlDataNodeInfo CONTRACT_SERVICE_MANAGER = new XmlDataNodeInfo("ServiceManager", "ContractTemplate/Service/ServiceManager");
        public static readonly XmlDataNodeInfo CONTRACT_SERVICE_PHONE = new XmlDataNodeInfo("ServicePhone", "ContractTemplate/Service/ServicePhone");

        public static readonly XmlDataNodeInfo CONTRACT_DEVICE_MODEL_NAME = new XmlDataNodeInfo("DeviceModelName", "ContractTemplate/Device/DeviceModel/DeviceModelName");
        public static readonly XmlDataNodeInfo CONTRACT_DEVICE_NUMBER = new XmlDataNodeInfo("DeviceNumber", "ContractTemplate/Device/DeviceNumber");
        public static readonly XmlDataNodeInfo CONTRACT_DEVICE_FISCAL_NUMBER = new XmlDataNodeInfo("DeviceFiscalNumber", "ContractTemplate/Device/DeviceFiscalNumber");


        // Certificate nodes
        public static readonly XmlDataNodeInfo CERTIFICATE_META_CURRENT_DATE = new XmlDataNodeInfo("MetaCurrentDate", "CertificateTemplate/Metadata/MetaCurrentDate");
        public static readonly XmlDataNodeInfo CERTIFICATE_META_CONTRACT_REF_NUMBER = new XmlDataNodeInfo("MetaContractRefNumber", "CertificateTemplate/Metadata/MetaContractRefNumber");
        public static readonly XmlDataNodeInfo CERTIFICATE_META_CONTRACT_START_DATE = new XmlDataNodeInfo("MetaContractStartDate", "CertificateTemplate/Metadata/MetaContractStartDate");

        public static readonly XmlDataNodeInfo CERTIFICATE_CLIENT_NAME = new XmlDataNodeInfo("ClientName", "CertificateTemplate/Client/ClientName");
        public static readonly XmlDataNodeInfo CERTIFICATE_CLIENT_IDENTIFIER = new XmlDataNodeInfo("ClientIdentifier", "CertificateTemplate/Client/ClientIdentifier");
        public static readonly XmlDataNodeInfo CERTIFICATE_CLIENT_ADDRESS = new XmlDataNodeInfo("ClientAddress", "CertificateTemplate/Client/ClientAddress");
        public static readonly XmlDataNodeInfo CERTIFICATE_CLIENT_SITE = new XmlDataNodeInfo("ClientSite", "CertificateTemplate/Client/ClientSite");
        public static readonly XmlDataNodeInfo CERTIFICATE_CLIENT_MANAGER = new XmlDataNodeInfo("ClientManager", "CertificateTemplate/Client/ClientManager");

        public static readonly XmlDataNodeInfo CERTIFICATE_SERVICE_NAME = new XmlDataNodeInfo("ServiceName", "CertificateTemplate/Service/ServiceName");
        public static readonly XmlDataNodeInfo CERTIFICATE_SERVICE_BULSTAT = new XmlDataNodeInfo("ServiceBulstat", "CertificateTemplate/Service/ServiceBulstat");
        public static readonly XmlDataNodeInfo CERTIFICATE_SERVICE_ADDRESS = new XmlDataNodeInfo("ServiceAddress", "CertificateTemplate/Service/ServiceAddress");
        public static readonly XmlDataNodeInfo CERTIFICATE_SERVICE_MANAGER = new XmlDataNodeInfo("ServiceManager", "CertificateTemplate/Service/ServiceManager");
        public static readonly XmlDataNodeInfo CERTIFICATE_SERVICE_PHONE = new XmlDataNodeInfo("ServicePhone", "CertificateTemplate/Service/ServicePhone");

        public static readonly XmlDataNodeInfo CERTIFICATE_DEVICE_MODEL_NAME = new XmlDataNodeInfo("DeviceModelName", "CertificateTemplate/Device/DeviceModel/DeviceModelName");
        public static readonly XmlDataNodeInfo CERTIFICATE_DEVICE_MODEL_CERTIFICATE = new XmlDataNodeInfo("DeviceModelCertificate", "CertificateTemplate/Device/DeviceModel/DeviceModelCertificate");
        public static readonly XmlDataNodeInfo CERTIFICATE_DEVICE_NUMBER = new XmlDataNodeInfo("DeviceNumber", "CertificateTemplate/Device/DeviceNumber");
        public static readonly XmlDataNodeInfo CERTIFICATE_DEVICE_FICSAL_NUMBER = new XmlDataNodeInfo("DeviceFiscalNumber", "CertificateTemplate/Device/DeviceFiscalNumber");
        public static readonly XmlDataNodeInfo CERTIFICATE_DEVICE_NAP_NUMBER = new XmlDataNodeInfo("DeviceNAPNumber", "CertificateTemplate/Device/DeviceNAP/DeviceNAPNumber");
        public static readonly XmlDataNodeInfo CERTIFICATE_DEVICE_NAP_DATE = new XmlDataNodeInfo("DeviceNAPDate", "CertificateTemplate/Device/DeviceNAP/DeviceNAPDate");


        // Protocol nodes
        public static readonly XmlDataNodeInfo PROTOCOL_МЕТА_CURRENT_DATE = new XmlDataNodeInfo("MetaCurrentDate", "ProtocolTemplate/Metadata/MetaCurrentDate");

        public static readonly XmlDataNodeInfo PROTOCOL_CLIENT_NAME = new XmlDataNodeInfo("ClientName", "ProtocolTemplate/Client/ClientName");
        public static readonly XmlDataNodeInfo PROTOCOL_CLIENT_BULSTAT = new XmlDataNodeInfo("ClientBulstat", "ProtocolTemplate/Client/ClientBulstat");
        public static readonly XmlDataNodeInfo PROTOCOL_CLIENT_ADDRESS = new XmlDataNodeInfo("ClientAddress", "ProtocolTemplate/Client/ClientAddress");
        public static readonly XmlDataNodeInfo PROTOCOL_CLIENT_SITE = new XmlDataNodeInfo("ClientSite", "ProtocolTemplate/Client/ClientSite");
        public static readonly XmlDataNodeInfo PROTOCOL_CLIENT_MANAGER = new XmlDataNodeInfo("ClientManager", "ProtocolTemplate/Client/ClientManager");
        public static readonly XmlDataNodeInfo PROTOCOL_CLIENT_TDD = new XmlDataNodeInfo("ClientTDD", "ProtocolTemplate/Client/ClientTDD");

        public static readonly XmlDataNodeInfo PROTOCOL_SERVICE_NAME = new XmlDataNodeInfo("ServiceName", "ProtocolTemplate/Service/ServiceName");
        public static readonly XmlDataNodeInfo PROTOCOL_SERVICE_BULSTAT = new XmlDataNodeInfo("ServiceBulstat", "ProtocolTemplate/Service/ServiceBulstat");
        public static readonly XmlDataNodeInfo PROTOCOL_SERVICE_ADDRESS = new XmlDataNodeInfo("ServiceAddress", "ProtocolTemplate/Service/ServiceAddress");
        public static readonly XmlDataNodeInfo PROTOCOL_SERVICE_MANAGER = new XmlDataNodeInfo("ServiceManager", "ProtocolTemplate/Service/ServiceManager");
        public static readonly XmlDataNodeInfo PROTOCOL_SERVICE_PHONE = new XmlDataNodeInfo("ServicePhone", "ProtocolTemplate/Service/ServicePhone");

        public static readonly XmlDataNodeInfo PROTOCOL_DEVICE_MODEL_NAME = new XmlDataNodeInfo("DeviceModelName", "ProtocolTemplate/Device/DeviceModel/DeviceModelName");
        public static readonly XmlDataNodeInfo PROTOCOL_DEVICE_MODEL_CERTIFICATE = new XmlDataNodeInfo("DeviceModelCertificate", "ProtocolTemplate/Device/DeviceModel/DeviceModelCertificate");
        public static readonly XmlDataNodeInfo PROTOCOL_DEVICE_NUMBER = new XmlDataNodeInfo("DeviceNumber", "ProtocolTemplate/Device/DeviceNumber");
        public static readonly XmlDataNodeInfo PROTOCOL_DEVICE_FISCAL_NUMBER = new XmlDataNodeInfo("DeviceFiscalNumber", "ProtocolTemplate/Device/DeviceFiscalNumber");
    }
}
