﻿using CashRegisterRepairs.Model;
using CashRegisterRepairs.Model.Exceptions;
using CashRegisterRepairs.Model.Extensions;
using CashRegisterRepairs.Utilities.Locators;
using System;
using System.Xml;
using static CashRegisterRepairs.Model.Extensions.EntityExtension.Template;

namespace CashRegisterRepairs.Utilities.Data.Xml
{
    public sealed class XmlDataGenerator
    {
        public static string BuildDataXml(Document document, int documentReferenceNumber)
        {
            XmlDocument templateXmlContent = new XmlDocument();
            templateXmlContent.LoadXml(document.Template.TEMPLATE_CONTENT);

            string[] serviceProfile = ResourceLocator.FetchServiceProfile();

            TemplateType type = TemplateExtensions.GetType(document.Template.TYPE);
            switch (type)
            {
                case TemplateType.Contract:
                    FillContractData(document, templateXmlContent, documentReferenceNumber, serviceProfile);
                    break;
                case TemplateType.Certificate:
                    FillCertificateData(document, templateXmlContent, documentReferenceNumber, serviceProfile);
                    break;
                case TemplateType.Protocol:
                    FillProtocolData(document, templateXmlContent, serviceProfile);
                    break;
                default:
                    break;
            }

            return templateXmlContent.OuterXml;
        }

        public static void FillContractData(Document document, XmlDocument template, int referenceNumber, string[] serviceProfile)
        {
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_META_CURRENT_DATE.XPath, DateTime.Today.Date.ToShortDateString());
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_META_REF_NUMBER.XPath, referenceNumber.ToString());
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_META_START_DATE.XPath, document.START_DATE.ToShortDateString());
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_META_END_DATE.XPath, document.END_DATE.ToShortDateString());
            //TODO: Refactor - set price dynamically OR extract constant price...
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_META_SUPPORT_FEE.XPath, "5500 лв.");

            fillDataNode(template, XmlDataNodeInfo.CONTRACT_CLIENT_NAME.XPath, document.Device.Site.Client.NAME);
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_CLIENT_BULSTAT.XPath, document.Device.Site.Client.BULSTAT);
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_CLIENT_ADDRESS.XPath, document.Device.Site.Client.ADDRESS);
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_CLIENT_MANAGER.XPath, document.Device.Site.Client.Manager.NAME);

            fillDataNode(template, XmlDataNodeInfo.CONTRACT_SERVICE_NAME.XPath, serviceProfile[ServiceProfile.NAME_IDX]);
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_SERVICE_ADDRESS.XPath, serviceProfile[ServiceProfile.ADDRESS_IDX]);
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_SERVICE_MANAGER.XPath, serviceProfile[ServiceProfile.MANAGER_IDX]);
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_SERVICE_PHONE.XPath, serviceProfile[ServiceProfile.PHONE_IDX]);

            fillDataNode(template, XmlDataNodeInfo.CONTRACT_DEVICE_MODEL_NAME.XPath, document.Device.DeviceModel.MODEL);
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_DEVICE_NUMBER.XPath, document.Device.DEVICE_NUM_POSTFIX);
            fillDataNode(template, XmlDataNodeInfo.CONTRACT_DEVICE_FISCAL_NUMBER.XPath, document.Device.FISCAL_NUM_POSTFIX);
        }

        public static void FillCertificateData(Document document, XmlDocument template, int referenceNumber, string[] serviceProfile)
        {
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_META_CURRENT_DATE.XPath, DateTime.Today.ToShortDateString());
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_META_CONTRACT_REF_NUMBER.XPath, referenceNumber.ToString());
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_META_CONTRACT_START_DATE.XPath, document.START_DATE.ToShortDateString());

            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_CLIENT_NAME.XPath, document.Device.Site.Client.NAME);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_CLIENT_IDENTIFIER.XPath, string.IsNullOrEmpty(document.Device.Site.Client.EGN) ? document.Device.Site.Client.BULSTAT : document.Device.Site.Client.EGN);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_CLIENT_ADDRESS.XPath, document.Device.Site.Client.ADDRESS);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_CLIENT_SITE.XPath, document.Device.Site.NAME);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_CLIENT_MANAGER.XPath, document.Device.Site.Client.Manager.NAME);

            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_SERVICE_NAME.XPath, serviceProfile[ServiceProfile.NAME_IDX]);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_SERVICE_BULSTAT.XPath, serviceProfile[ServiceProfile.BULSTAT_IDX]);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_SERVICE_ADDRESS.XPath, serviceProfile[ServiceProfile.ADDRESS_IDX]);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_SERVICE_MANAGER.XPath, serviceProfile[ServiceProfile.MANAGER_IDX]);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_SERVICE_PHONE.XPath, serviceProfile[ServiceProfile.PHONE_IDX]);

            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_DEVICE_MODEL_NAME.XPath, document.Device.DeviceModel.MODEL);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_DEVICE_MODEL_CERTIFICATE.XPath, document.Device.DeviceModel.CERTIFICATE);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_DEVICE_NAP_NUMBER.XPath, document.Device.NAP_NUMBER);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_DEVICE_NAP_DATE.XPath, document.Device.NAP_DATE.ToShortDateString());
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_DEVICE_NUMBER.XPath, document.Device.DeviceModel.DEVICE_NUM_PREFIX + document.Device.DEVICE_NUM_POSTFIX);
            fillDataNode(template, XmlDataNodeInfo.CERTIFICATE_DEVICE_FICSAL_NUMBER.XPath, document.Device.DeviceModel.FISCAL_NUM_PREFIX + document.Device.FISCAL_NUM_POSTFIX);
        }

        public static void FillProtocolData(Document document, XmlDocument template, string[] serviceProfile)
        {
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_МЕТА_CURRENT_DATE.XPath, DateTime.Today.ToShortDateString());

            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_CLIENT_NAME.XPath, document.Device.Site.Client.NAME);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_CLIENT_BULSTAT.XPath, document.Device.Site.Client.BULSTAT);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_CLIENT_ADDRESS.XPath, document.Device.Site.Client.ADDRESS);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_CLIENT_SITE.XPath, document.Device.Site.NAME);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_CLIENT_MANAGER.XPath, document.Device.Site.Client.Manager.NAME);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_CLIENT_TDD.XPath, document.Device.Site.Client.TDD);

            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_SERVICE_NAME.XPath, serviceProfile[ServiceProfile.NAME_IDX]);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_SERVICE_BULSTAT.XPath, serviceProfile[ServiceProfile.BULSTAT_IDX]);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_SERVICE_ADDRESS.XPath, serviceProfile[ServiceProfile.ADDRESS_IDX]);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_SERVICE_MANAGER.XPath, serviceProfile[ServiceProfile.MANAGER_IDX]);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_SERVICE_PHONE.XPath, serviceProfile[ServiceProfile.PHONE_IDX]);

            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_DEVICE_MODEL_NAME.XPath, document.Device.DeviceModel.MODEL);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_DEVICE_MODEL_CERTIFICATE.XPath, document.Device.DeviceModel.CERTIFICATE);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_DEVICE_NUMBER.XPath, document.Device.DeviceModel.DEVICE_NUM_PREFIX + document.Device.DEVICE_NUM_POSTFIX);
            fillDataNode(template, XmlDataNodeInfo.PROTOCOL_DEVICE_FISCAL_NUMBER.XPath, document.Device.DeviceModel.FISCAL_NUM_PREFIX + document.Device.FISCAL_NUM_POSTFIX);
        }

        private static void fillDataNode(XmlDocument template, string nodePath, string nodeValue)
        {
            try
            {
                template.SelectSingleNode(nodePath).InnerText = nodeValue;
            }
            catch (NullReferenceException nre)
            {
                int missingFieldStartIdx = nodePath.LastIndexOf(@"/") + 1;
                string missingXmlField = nodePath.Substring(missingFieldStartIdx);

                throw new MissingDataXmlFieldException(missingXmlField, nodePath, nre);
            }
        }
    }
}
