﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace CashRegisterRepairs.Utilities.Security
{
    public class LoginHandler : ILoginHandler
    {
        //TODO: Refactor USER / PW - keep in file(protected file)...
        private const string USER = "Rosen";
        private const string PASS = "1234";

        public bool Login(string username, SecureString password)
        {
            IntPtr passwordBSTR = default(IntPtr);
            string insecurePassword = "";
            try
            {
                passwordBSTR = Marshal.SecureStringToBSTR(password);
                insecurePassword = Marshal.PtrToStringBSTR(passwordBSTR);
            }
            catch
            {
                insecurePassword = "";
            }
            return MockServiceProxyCall(username, insecurePassword);
        }

        private bool MockServiceProxyCall(string username, string password)
        {
            if (username == USER && password == PASS) return true;
            else return false;
        }
    }
}
