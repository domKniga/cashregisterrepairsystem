﻿using CashRegisterRepairs.Model.Exceptions;
using MahApps.Metro.Controls;
using System.Collections.Generic;
using System.Linq;

namespace CashRegisterRepairs.Utilities.Locators
{
    public sealed class ViewLocator
    {
        // Ids according to defined values in XAML for the corresponding views(see action for event Closing)
        public const string ADDITION_VIEW_CLIENTS_ID = "clients";
        public const string ADDITION_VIEW_SITES_ID = "sites";
        public const string ADDITION_VIEW_DEVICES_ID = "devices";
        public const string ADDITION_VIEW_MODELS_ID = "models";

        public const int TAB_VIEW_CLIENTS_SITES_IDX = 0;
        public const int TAB_VIEW_MODELS_DEVICES_IDX = 1;
        public const int TAB_VIEW_TEMPLATES_DOCUMENTS_IDX = 2;
        public const int TAB_VIEW_SERVICE_INFO_IDX = 3;

        public static List<MetroWindow> FindAllViews()
        {
            return App.Current.Windows.OfType<MetroWindow>().ToList();
        }

        public static MetroWindow FindView<T>()
        {
            foreach (var view in App.Current.Windows)
            {
                if (view.GetType().Equals(typeof(T)))
                {
                    return (view as MetroWindow);
                }
            }

            throw new MissingViewException(typeof(T).ToString());
        }

        public static MetroWindow GetMainView()
        {
            return (App.Current.MainWindow as MetroWindow);
        }
    }
}
