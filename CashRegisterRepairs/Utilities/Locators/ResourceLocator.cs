﻿using CashRegisterRepairs.Model;
using CashRegisterRepairs.Model.Exceptions;
using CashRegisterRepairs.Model.Extensions;
using System;
using System.IO;
using System.Reflection;
using static CashRegisterRepairs.Model.Extensions.EntityExtension.Template;

namespace CashRegisterRepairs.Utilities.Locators
{
    public sealed class ResourceLocator
    {
        public static readonly string serviceProfilePath = ResolveAppPath() + @"\Resources\Service.txt";
        public static readonly string temporaryDocumentsPath = ResolveAppPath() + @"\Resources\TemporaryDocuments\";

        public static readonly string contractWordTemplatePath = ResolveAppPath() + @"\Resources\WordTemplates\ContractTemplate.dotx";
        public static readonly string certificateWordTemplatePath = ResolveAppPath() + @"\Resources\WordTemplates\CertificateTemplate.dotx";
        public static readonly string protocolWordTemplatePath = ResolveAppPath() + @"\Resources\WordTemplates\ProtocolTemplate.dotx";

        public const string TMP_DOC_EXTENSION = ".doc";

        public static string[] FetchServiceProfile()
        {
            if (File.Exists(serviceProfilePath) && new FileInfo(serviceProfilePath).Length != 0)
            {
                string[] profile = File.ReadAllText(serviceProfilePath).Split(ServiceProfile.IDX_SEPARATOR);
                if (ServiceProfile.isCompleteProfile(profile))
                {
                    return profile;
                }
            }

            throw new MissingServiceProfileInfoException("Липсва информация за поле/та от профила на сервиза!");
        }

        public static string FetchTemporaryWordDocumentFullPath()
        {
            return temporaryDocumentsPath + Guid.NewGuid() + TMP_DOC_EXTENSION;
        }

        public static string FetchWordTemplatePath(string templateType)
        {
            TemplateType type = TemplateExtensions.GetType(templateType);
            switch (type)
            {
                case TemplateType.Contract:
                    return contractWordTemplatePath;
                case TemplateType.Certificate:
                    return certificateWordTemplatePath;
                case TemplateType.Protocol:
                    return protocolWordTemplatePath;
                default:
                    return string.Empty;
            }
        }

        private static string ResolveAppPath()
        {
            string assemblyPathNode = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string binPathNode = Directory.GetParent(assemblyPathNode).FullName;
            return Directory.GetParent(binPathNode).FullName;
        }

    }
}
