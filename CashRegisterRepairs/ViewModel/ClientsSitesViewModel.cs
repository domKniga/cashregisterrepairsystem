﻿using CashRegisterRepairs.Model;
using CashRegisterRepairs.Model.Validation;
using CashRegisterRepairs.Utilities.Locators;
using CashRegisterRepairs.View;
using CashRegisterRepairs.ViewModel.Common;
using CashRegisterRepairs.ViewModel.Common.Interfaces;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace CashRegisterRepairs.ViewModel
{
    public class ClientsSitesViewModel : INotifyPropertyChanged, IAdditionViewModel
    {
        // FIELDS & COLLECTIONS
        #region FIELDS
        private readonly MetroWindow placeholder;
        private readonly PlaceholderViewModel tabNavigator;
        private readonly CashRegisterServiceContext dbModel;
        private bool canExecuteCommand = true; // command enable/disable
        private bool canOpenSubviewForm = true; // addition forms enable/disable
        private bool isCommitExecuted = false; // flag whether the commit was executed or not
        #endregion

        #region COLLECTIONS
        #region Local Storage(caches)
        private List<Manager> managersCache;
        private List<Client> clientsCache;
        private List<Site> sitesCache;
        private List<Device> devicesCache;
        #endregion

        #region Grid filling collections
        private ObservableCollection<Client> _clients;
        public ObservableCollection<Client> Clients
        {
            get { return _clients; }
            set { _clients = value; }
        }

        private ObservableCollection<Site> _sites;
        public ObservableCollection<Site> Sites
        {
            get { return _sites; }
            set { _sites = value; }
        }
        #endregion

        #region Combo box filler collections for DEVICES VIEW
        public List<string> ModelsList { get; private set; }
        public List<string> SitesList { get; private set; }
        #endregion
        #endregion

        public ClientsSitesViewModel(PlaceholderViewModel tabNav)
        {
            // Initialize DB context
            dbModel = new CashRegisterServiceContext();
            placeholder = ViewLocator.GetMainView();
            tabNavigator = tabNav;

            // Initializing datagrid backing collections
            _sites = new ObservableCollection<Site>();
            _clients = new ObservableCollection<Client>(dbModel.Clients);

            // Initialize caches
            managersCache = new List<Manager>();
            clientsCache = new List<Client>();
            sitesCache = new List<Site>();
            devicesCache = new List<Device>();

            // Generic commands
            EnableSubviewDisplayCommand = new TemplateCommand(EnableSubview, param => this.canExecuteCommand);
            DisableSubviewDisplayCommand = new TemplateCommand(DisableSubview, param => this.canExecuteCommand);
            DisplaySitesCommand = new TemplateCommand(RefreshSites, param => this.canExecuteCommand);

            // Additions commands
            AddClientCommand = new TemplateCommand(ShowClientsAdditionForm, param => this.canExecuteCommand);
            AddSiteCommand = new TemplateCommand(ShowSitesAdditionForm, param => this.canExecuteCommand);
            AddDeviceCommand = new TemplateCommand(ShowDevicesAdditionForm, param => this.canExecuteCommand);

            // Clients commands
            SaveClientAndManagerCommand = new TemplateCommand(SaveClient, param => this.canExecuteCommand);
            CommitClientsAndManagersCommand = new TemplateCommand(CommitClients, param => this.canExecuteCommand);
            RefreshClientsCommand = new TemplateCommand(RefreshClients, param => this.canExecuteCommand);

            // Sites commands
            SaveSiteCommand = new TemplateCommand(SaveSite, param => this.canExecuteCommand);
            CommitSiteCommand = new TemplateCommand(CommitSites, param => this.canExecuteCommand);
            RefreshSitesCommand = new TemplateCommand(RefreshSites, param => this.canExecuteCommand);

            // Devices commands
            SaveDeviceCommand = new TemplateCommand(SaveDevice, param => this.canExecuteCommand);
            CommitDevicesCommand = new TemplateCommand(CommitDevices, param => this.canExecuteCommand);
        }

        // METHODS
        #region METHODS
        #region Addition related methods implementation
        public void EnableSubview(object comingFromForm)
        {
            canOpenSubviewForm = true;

            ClearCaches();

            ShowAdditionCount(comingFromForm as string);
        }

        public void DisableSubview(object commandParameter)
        {
            canOpenSubviewForm = false;
        }

        public void ClearCaches()
        {
            clientsCache.Clear();
            managersCache.Clear();
            sitesCache.Clear();
            devicesCache.Clear();
        }

        public async void ShowAdditionCount(string formIdentifier)
        {
            int newEntries = 0;

            if (!isCommitExecuted)
            {
                await placeholder.ShowMessageAsync("ИНФО", "Няма добавени записи!");
                return;
            }

            switch (formIdentifier)
            {
                case ViewLocator.ADDITION_VIEW_CLIENTS_ID:
                    newEntries = clientsCache.Count;
                    ClearFieldsClients();
                    break;
                case ViewLocator.ADDITION_VIEW_SITES_ID:
                    newEntries = sitesCache.Count;
                    ClearFieldsSites();
                    break;
                case ViewLocator.ADDITION_VIEW_DEVICES_ID:
                    newEntries = devicesCache.Count;
                    ClearFieldsDevices();
                    break;
                default:
                    break;
            }

            if (newEntries > 0)
            {
                await placeholder.ShowMessageAsync("ИНФО", "Добавени " + newEntries + " записа!");
                isCommitExecuted = false;
            }
        }
        #endregion

        #region Grid loading methods
        private void RefreshClients(object commandParameter)
        {
            Clients.Clear();

            dbModel.Clients.ToList().ForEach(Clients.Add);
        }

        private async void RefreshSites(object commandParameter)
        {
            Sites.Clear();

            Client selectedClientFromGrid = (SelectedClient as Client);
            if (selectedClientFromGrid == null)
            {
                await placeholder.ShowMessageAsync("ГРЕШКА", "Няма избран клиент!");
                return;
            }

            dbModel.Sites.Where(s => s.CLIENT_ID == selectedClientFromGrid.ID).ToList().ForEach(Sites.Add);
        }
        #endregion

        #region Display addition form methods
        private void ShowClientsAdditionForm(object clientsTabDataContext)
        {
            if (canOpenSubviewForm)
            {
                AddClientView addClientsView = new AddClientView();
                addClientsView.DataContext = clientsTabDataContext;
                addClientsView.Show();
            }
        }

        private void ShowSitesAdditionForm(object clientsTabDataContext)
        {
            if (canOpenSubviewForm)
            {
                if (SelectedClient == null)
                {
                    placeholder.ShowMessageAsync("ИНФО", "Няма избран клиент!");
                    return;
                }

                AddSiteView addSitesView = new AddSiteView();
                addSitesView.DataContext = clientsTabDataContext;
                addSitesView.Show();
            }
        }

        private void ShowDevicesAdditionForm(object clientsTabDataContext)
        {
            // Initialize & Fill combo boxes for devices view
            SitesList = new List<string>();
            ModelsList = new List<string>();
            dbModel.Sites.ToList().ForEach(site => SitesList.Add(site.NAME));
            dbModel.DeviceModels.ToList().ForEach(model => ModelsList.Add(model.MODEL));

            // Set the selected site name to the combo box
            SelectedSiteName = (SelectedSite as Site).NAME;

            if (canOpenSubviewForm)
            {
                AddDeviceView addDevicesView = new AddDeviceView();
                addDevicesView.DataContext = clientsTabDataContext;
                addDevicesView.Show();
            }
        }
        #endregion

        #region Clear field methods
        private void ClearFieldsClients()
        {
            NAME = string.Empty;
            EGN = string.Empty;
            TDD = string.Empty;
            ADDRESS = string.Empty;
            BULSTAT = string.Empty;
            COMMENT = string.Empty;
            MANAGER = string.Empty;
            PHONE = string.Empty;
        }

        private void ClearFieldsSites()
        {
            SiteName = string.Empty;
            SiteAddress = string.Empty;
            SitePhone = string.Empty;
        }

        private void ClearFieldsDevices()
        {
            SelectedDeviceModel = string.Empty;
            SIM = string.Empty;
            DEVICE_NUM_POSTFIX = string.Empty;
            FISCAL_NUM_POSTFIX = string.Empty;
            NAP_DATE = null;
            NAP_NUMBER = string.Empty;
        }
        #endregion

        #region Clients(SAVE+COMMIT)
        public async void SaveClient(object commandParameter)
        {
            MetroWindow addClientsView = ViewLocator.FindView<AddClientView>();

            bool IsNeitherPersonNorCompany = string.IsNullOrEmpty(EGN) && string.IsNullOrEmpty(BULSTAT);
            bool IsBothPersonAndCompany = !string.IsNullOrEmpty(EGN) && !string.IsNullOrEmpty(BULSTAT);

            // TODO: Confirm logic - settings are either/or, both should not be set(see above checks)...
            if (IsNeitherPersonNorCompany || IsBothPersonAndCompany)
            {
                await addClientsView.ShowMessageAsync("ГРЕШКА", "Невалидни/невъведени данни - клиентът трябва да е физическо лице(ЕГН) ИЛИ фирма(булстат)!");
                return;
            }

            Manager manager = new Manager();
            manager.NAME = MANAGER;
            manager.PHONE = PHONE;

            Client client = new Client();
            client.EGN = EGN;
            client.NAME = NAME;
            client.TDD = TDD;
            client.ADDRESS = ADDRESS;
            client.BULSTAT = BULSTAT;
            client.COMMENT = COMMENT;
            client.Manager = manager;

            ICollection<ValidationResult> validationErrors;
            if (EntityValidator.TryValidateEntity(manager, out validationErrors) && EntityValidator.TryValidateEntity(client, out validationErrors))
            {
                managersCache.Add(manager);
                clientsCache.Add(client);
                ClearFieldsClients();
            }
            else
            {
                await addClientsView.ShowMessageAsync("ГРЕШКА", string.Join("\n", validationErrors));
            }
        }

        public async void CommitClients(object commandParameter)
        {
            MetroWindow addClientsView = ViewLocator.FindView<AddClientView>();

            try
            {
                managersCache.ForEach(manager => dbModel.Managers.Add(manager));
                clientsCache.ForEach(client => dbModel.Clients.Add(client));
                dbModel.SaveChanges();

                isCommitExecuted = true;

                RefreshClients(null);
            }
            catch (DataException de)
            {
                await addClientsView.ShowMessageAsync("ГРЕШКА", "ПРОБЛЕМ СЪС ЗАПАЗВАНЕТО В БД: \n" + de.GetBaseException().Message);
            }
            finally
            {
                managersCache.Clear();
                clientsCache.Clear();
                addClientsView.Close();
            }
        }
        #endregion

        #region Sites(SAVE+COMMIT)
        private async void SaveSite(object commandParameter)
        {
            MetroWindow addSitesView = ViewLocator.FindView<AddSiteView>();

            Site site = new Site();
            site.NAME = SiteName;
            site.ADDRESS = SiteAddress;
            site.PHONE = SitePhone;
            site.Client = dbModel.Clients.Find((SelectedClient as Client).ID);

            ICollection<ValidationResult> validationErrors;
            if (EntityValidator.TryValidateEntity(site, out validationErrors))
            {
                sitesCache.Add(site);
                ClearFieldsSites();
            }
            else
            {
                await addSitesView.ShowMessageAsync("ГРЕШКА", string.Join("\n", validationErrors));
            }
        }

        private async void CommitSites(object commandParameter)
        {
            MetroWindow addSitesView = ViewLocator.FindView<AddSiteView>();

            try
            {
                sitesCache.ForEach(site => dbModel.Sites.Add(site));
                dbModel.SaveChanges();

                isCommitExecuted = true;

                RefreshSites(null);
            }
            catch (DataException de)
            {
                await addSitesView.ShowMessageAsync("ГРЕШКА", "ПРОБЛЕМ СЪС ЗАПАЗВАНЕТО В БД: \n" + de.GetBaseException().Message);
            }
            finally
            {
                sitesCache.Clear();
                addSitesView.Close();
            }
        }
        #endregion

        #region Devices(SAVE+COMMIT)
        public async void SaveDevice(object commandParameter)
        {
            MetroWindow addDevicesView = ViewLocator.FindView<AddDeviceView>();

            Device device = new Device();
            device.SIM = SIM;
            device.DEVICE_NUM_POSTFIX = DEVICE_NUM_POSTFIX;
            device.FISCAL_NUM_POSTFIX = FISCAL_NUM_POSTFIX;
            device.NAP_NUMBER = NAP_NUMBER;
            device.NAP_DATE = NAP_DATE ?? DateTime.Today;
            device.Site = dbModel.Sites.Find((SelectedSite as Site).ID);
            device.DeviceModel = dbModel.DeviceModels.ToList().Where(model => model.MODEL == SelectedDeviceModel).FirstOrDefault();

            ICollection<ValidationResult> validationErrors;
            if (EntityValidator.TryValidateEntity(device, out validationErrors))
            {
                devicesCache.Add(device);
                ClearFieldsDevices();
            }
            else
            {
                await addDevicesView.ShowMessageAsync("ГРЕШКА", string.Join("\n", validationErrors));
            }
        }

        public async void CommitDevices(object commandParameter)
        {
            MetroWindow addDevicesView = ViewLocator.FindView<AddDeviceView>();

            try
            {
                devicesCache.ForEach(device => dbModel.Devices.Add(device));
                dbModel.SaveChanges();

                isCommitExecuted = true;
            }
            catch (DataException de)
            {
                await addDevicesView.ShowMessageAsync("ГРЕШКА", "ПРОБЛЕМ СЪС ЗАПАЗВАНЕТО В БД: \n" + de.GetBaseException().Message);
            }
            finally
            {
                devicesCache.Clear();
                addDevicesView.Close();
                tabNavigator.SelectedTab = ViewLocator.TAB_VIEW_MODELS_DEVICES_IDX;
            }
        }
        #endregion
        #endregion

        // COMMANDS
        #region COMMANDS
        private ICommand _enableSubviewDisplayCommand;
        public ICommand EnableSubviewDisplayCommand
        {
            get { return _enableSubviewDisplayCommand; }
            set { _enableSubviewDisplayCommand = value; }
        }

        private ICommand _disableSubviewDisplayCommand;
        public ICommand DisableSubviewDisplayCommand
        {
            get { return _disableSubviewDisplayCommand; }
            set { _disableSubviewDisplayCommand = value; }
        }
        #region Client commands
        private ICommand _addClientCommand;
        public ICommand AddClientCommand
        {
            get { return _addClientCommand; }
            set { _addClientCommand = value; }
        }

        private ICommand _saveClientAndManagerCommand;
        public ICommand SaveClientAndManagerCommand
        {
            get { return _saveClientAndManagerCommand; }
            set { _saveClientAndManagerCommand = value; }
        }

        private ICommand _commitClientsAndManagersCommand;
        public ICommand CommitClientsAndManagersCommand
        {
            get { return _commitClientsAndManagersCommand; }
            set { _commitClientsAndManagersCommand = value; }
        }

        private ICommand _refreshClientsCommand;
        public ICommand RefreshClientsCommand
        {
            get { return _refreshClientsCommand; }
            set { _refreshClientsCommand = value; }
        }
        #endregion

        #region Site commands
        private ICommand _displaySitesCommand;
        public ICommand DisplaySitesCommand
        {
            get { return _displaySitesCommand; }
            set { _displaySitesCommand = value; }
        }

        private ICommand _addSiteCommand;
        public ICommand AddSiteCommand
        {
            get { return _addSiteCommand; }
            set { _addSiteCommand = value; }
        }

        private ICommand _saveSiteCommand;
        public ICommand SaveSiteCommand
        {
            get { return _saveSiteCommand; }
            set { _saveSiteCommand = value; }
        }

        private ICommand _commitSiteCommand;
        public ICommand CommitSiteCommand
        {
            get { return _commitSiteCommand; }
            set { _commitSiteCommand = value; }
        }

        private ICommand _refreshSitesCommand;
        public ICommand RefreshSitesCommand
        {
            get { return _refreshSitesCommand; }
            set { _refreshSitesCommand = value; }
        }
        #endregion

        #region Device commands
        private ICommand _addDeviceCommand;
        public ICommand AddDeviceCommand
        {
            get { return _addDeviceCommand; }
            set { _addDeviceCommand = value; }
        }

        private ICommand _saveDeviceCommand;
        public ICommand SaveDeviceCommand
        {
            get { return _saveDeviceCommand; }
            set { _saveDeviceCommand = value; }
        }

        private ICommand _commitDevicesCommand;
        public ICommand CommitDevicesCommand
        {
            get { return _commitDevicesCommand; }
            set { _commitDevicesCommand = value; }
        }
        #endregion
        #endregion

        // SELECTION PROPERTIES (CURRENT CONTEXT)
        #region SELECTION PROPERTIES (CURRENT CONTEXT)
        private string _selectedSiteName = string.Empty;
        public string SelectedSiteName
        {
            get { return _selectedSiteName; }
            set { _selectedSiteName = value; NotifyPropertyChanged(); }
        }

        private string _selectedDeviceModel = string.Empty;
        public string SelectedDeviceModel
        {
            get { return _selectedDeviceModel; }
            set { _selectedDeviceModel = value; NotifyPropertyChanged(); }
        }

        private object _selectedClientFromGrid;
        public object SelectedClient
        {
            get { return _selectedClientFromGrid; }
            set { _selectedClientFromGrid = value; NotifyPropertyChanged(); }
        }

        private object _selectedSiteFromGrid;
        public object SelectedSite
        {
            get { return _selectedSiteFromGrid; }
            set { _selectedSiteFromGrid = value; NotifyPropertyChanged(); }
        }
        #endregion

        // PROPERTIES
        #region PROPERTIES

        #region Client properties
        private string _clientName = string.Empty;
        public string NAME
        {
            get { return _clientName; }
            set { _clientName = value; NotifyPropertyChanged(); }
        }

        private string _clientEGN = string.Empty;
        public string EGN
        {
            get { return _clientEGN; }
            set { _clientEGN = value; NotifyPropertyChanged(); }
        }

        private string _clientBulstat = string.Empty;
        public string BULSTAT
        {
            get { return _clientBulstat; }
            set { _clientBulstat = value; NotifyPropertyChanged(); }
        }

        private string _clientAddress = string.Empty;
        public string ADDRESS
        {
            get { return _clientAddress; }
            set { _clientAddress = value; NotifyPropertyChanged(); }
        }

        private string _clientTDD = string.Empty;
        public string TDD
        {
            get { return _clientTDD; }
            set { _clientTDD = value; NotifyPropertyChanged(); }
        }

        private string _managerName = string.Empty;
        public string MANAGER
        {
            get { return _managerName; }
            set { _managerName = value; NotifyPropertyChanged(); }
        }

        private string _managerPhone = string.Empty;
        public string PHONE
        {
            get { return _managerPhone; }
            set { _managerPhone = value; NotifyPropertyChanged(); }
        }

        private string _clientComment = string.Empty;
        public string COMMENT
        {
            get { return _clientComment; }
            set { _clientComment = value; NotifyPropertyChanged(); }
        }
        #endregion

        #region Site properties
        private string _siteName = string.Empty;
        public string SiteName
        {
            get { return _siteName; }
            set { _siteName = value; NotifyPropertyChanged(); }
        }

        private string _siteAddress = string.Empty;
        public string SiteAddress
        {
            get { return _siteAddress; }
            set { _siteAddress = value; NotifyPropertyChanged(); }
        }

        private string _sitePhone = string.Empty;
        public string SitePhone
        {
            get { return _sitePhone; }
            set { _sitePhone = value; NotifyPropertyChanged(); }
        }
        #endregion

        #region Device properties
        private string _deviceNumber = string.Empty;
        public string DEVICE_NUM_POSTFIX
        {
            get { return _deviceNumber; }
            set { _deviceNumber = value; NotifyPropertyChanged(); }
        }

        private string _deviceFiscalNumer = string.Empty;
        public string FISCAL_NUM_POSTFIX
        {
            get { return _deviceFiscalNumer; }
            set { _deviceFiscalNumer = value; NotifyPropertyChanged(); }
        }

        private string _deviceSIM = string.Empty;
        public string SIM
        {
            get { return _deviceSIM; }
            set { _deviceSIM = value; NotifyPropertyChanged(); }
        }

        private string _napNumber = string.Empty;
        public string NAP_NUMBER
        {
            get { return _napNumber; }
            set { _napNumber = value; NotifyPropertyChanged(); }
        }

        private DateTime? _napDate;
        public DateTime? NAP_DATE
        {
            get { return _napDate; }
            set { _napDate = value; NotifyPropertyChanged(); }
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        #endregion
    }
}
