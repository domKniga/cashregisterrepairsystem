﻿namespace CashRegisterRepairs.ViewModel.Common.Interfaces
{
    public interface IAdditionViewModel : IViewModel
    {
        void ClearCaches();
        void EnableSubview(object comingFromForm);
        void DisableSubview(object commandParameter);
        void ShowAdditionCount(string formIdentifier);
    }
}
