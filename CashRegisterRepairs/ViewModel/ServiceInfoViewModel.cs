﻿using CashRegisterRepairs.Model;
using CashRegisterRepairs.Model.Exceptions;
using CashRegisterRepairs.Model.Validation;
using CashRegisterRepairs.Utilities.Locators;
using CashRegisterRepairs.ViewModel.Common;
using CashRegisterRepairs.ViewModel.Common.Interfaces;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace CashRegisterRepairs.ViewModel
{
    public class ServiceInfoViewModel : INotifyPropertyChanged, IViewModel
    {
        //CONSTANTS
        #region CONSTANTS
        private readonly CultureInfo DEFAULT_CULTURE = new CultureInfo("en-US");
        private readonly MetroWindow placeholder;
        #endregion

        // FIELDS
        #region FIELDS
        private Location cachedServiceLocation;
        private string[] serviceProfileItems;
        #endregion

        public ServiceInfoViewModel()
        {
            placeholder = ViewLocator.GetMainView();

            // Load current profile
            LoadServiceProfile();

            // Load current location
            // Parse irregardless of system settings
            double latitude = double.Parse(ProfileDisplay.Latitude, DEFAULT_CULTURE.NumberFormat);
            double longtitude = double.Parse(ProfileDisplay.Longtitude, DEFAULT_CULTURE.NumberFormat);
            ServiceLocation = new Location(latitude, longtitude);
            cachedServiceLocation = ServiceLocation;

            // Disable editing at first
            IsMapEnabled = false;
            IsTextBoxFocusable = false;
            IsTextBoxUnmodifable = true;

            // Initialize commands
            EnableEditingCommand = new TemplateCommand(EnableEditing, param => this.canExecute);
            SaveServiceProfileCommand = new TemplateCommand(SaveServiceProfile, param => this.canExecute);
            ChangeLocationCommand = new TemplateCommand(ChangeLocationOnMouseDoubleClick, param => this.canExecute);
            ResetLocationCommand = new TemplateCommand(ResetLocation, param => this.canExecute);
        }

        // METHODS
        #region METHODS
        private void ChangeLocationOnMouseDoubleClick(object mouseEventArgs)
        {
            MouseButtonEventArgs mouseEventContext = mouseEventArgs as MouseButtonEventArgs;

            Map currentMap = mouseEventContext.Source as Map;
            Point mousePosition = mouseEventContext.GetPosition(currentMap);

            ServiceLocation = currentMap.ViewportPointToLocation(mousePosition);
            ProfileDisplay.Latitude = ServiceLocation.Latitude.ToString(DEFAULT_CULTURE.NumberFormat);
            ProfileDisplay.Longtitude = ServiceLocation.Longitude.ToString(DEFAULT_CULTURE.NumberFormat);
        }

        private void ResetLocation(object commandParameter)
        {
            ServiceLocation = cachedServiceLocation;
        }

        private void LoadServiceProfile()
        {
            try
            {
                serviceProfileItems = ResourceLocator.FetchServiceProfile();

                ProfileDisplay = new ServiceProfile();
                ProfileDisplay.Name = serviceProfileItems[ServiceProfile.NAME_IDX];
                ProfileDisplay.Bulstat = serviceProfileItems[ServiceProfile.BULSTAT_IDX];
                ProfileDisplay.Address = serviceProfileItems[ServiceProfile.ADDRESS_IDX];
                ProfileDisplay.Manager = serviceProfileItems[ServiceProfile.MANAGER_IDX];
                ProfileDisplay.Phone = serviceProfileItems[ServiceProfile.PHONE_IDX];
                ProfileDisplay.Latitude = serviceProfileItems[ServiceProfile.LATITUDE_IDX];
                ProfileDisplay.Longtitude = serviceProfileItems[ServiceProfile.LONGTITUDE_IDX];
            }
            catch (MissingServiceProfileInfoException mspie)
            {
                MessageBox.Show(mspie.Message, "ГРЕШКА", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1);
            }
        }

        private void EnableEditing(object commandParameter)
        {
            IsMapEnabled = true;
            IsTextBoxFocusable = true;
            IsTextBoxUnmodifable = false;
        }

        private async void SaveServiceProfile(object commandParameter)
        {
            ICollection<ValidationResult> validationErrors;
            if (!EntityValidator.TryValidateEntity(ProfileDisplay, out validationErrors))
            {
                await placeholder.ShowMessageAsync("ГРЕШКА", string.Join("\n", validationErrors));
                return;
            }

            MessageDialogResult choice = await placeholder.ShowMessageAsync("ПОТВЪРЖДЕНИЕ", "Наистина ли искате да запазите тези промени?", MessageDialogStyle.AffirmativeAndNegative);
            if (choice == MessageDialogResult.Affirmative)
            {
                StringBuilder profileBuilder = new StringBuilder(string.Join(ServiceProfile.IDX_SEPARATOR.ToString(), serviceProfileItems).Trim());
                profileBuilder.Replace(serviceProfileItems[ServiceProfile.NAME_IDX], ProfileDisplay.Name.Trim())
                .Replace(serviceProfileItems[ServiceProfile.BULSTAT_IDX], ProfileDisplay.Bulstat.Trim())
                .Replace(serviceProfileItems[ServiceProfile.ADDRESS_IDX], ProfileDisplay.Address.Trim())
                .Replace(serviceProfileItems[ServiceProfile.MANAGER_IDX], ProfileDisplay.Manager.Trim())
                .Replace(serviceProfileItems[ServiceProfile.PHONE_IDX], ProfileDisplay.Phone.Trim())
                .Replace(serviceProfileItems[ServiceProfile.LATITUDE_IDX], ProfileDisplay.Latitude.Trim())
                .Replace(serviceProfileItems[ServiceProfile.LONGTITUDE_IDX], ProfileDisplay.Longtitude.Trim());

                File.WriteAllText(ResourceLocator.serviceProfilePath, profileBuilder.ToString().Trim());

                cachedServiceLocation = ServiceLocation;

                await placeholder.ShowMessageAsync("ПРОМЯНА", "Промените по профила са запазени!");
            }

            IsMapEnabled = false;
            IsTextBoxFocusable = false;
            IsTextBoxUnmodifable = true;
        }
        #endregion

        // COMMANDS
        #region COMMANDS
        private ICommand _enableEditingCommand;
        public ICommand EnableEditingCommand
        {
            get { return _enableEditingCommand; }
            set { _enableEditingCommand = value; }
        }

        private ICommand _saveServiceProfileCommand;
        public ICommand SaveServiceProfileCommand
        {
            get { return _saveServiceProfileCommand; }
            set { _saveServiceProfileCommand = value; }
        }

        private ICommand _changeLocationCommand;
        public ICommand ChangeLocationCommand
        {
            get { return _changeLocationCommand; }
            set { _changeLocationCommand = value; }
        }

        private ICommand _resetLocationCommand;
        public ICommand ResetLocationCommand
        {
            get { return _resetLocationCommand; }
            set { _resetLocationCommand = value; }
        }
        #endregion

        // PROPERTIES
        #region PROPERTIES
        private Location _serviceLocation;
        public Location ServiceLocation
        {
            get { return _serviceLocation; }
            set { _serviceLocation = value; NotifyPropertyChanged(); }
        }

        private ServiceProfile _profileDisplay;
        public ServiceProfile ProfileDisplay
        {
            get { return _profileDisplay; }
            set { _profileDisplay = value; NotifyPropertyChanged(); }
        }

        private bool _isUnmodifable;
        public bool IsTextBoxUnmodifable
        {
            get { return _isUnmodifable; }
            set { _isUnmodifable = value; NotifyPropertyChanged(); }
        }

        private bool _isFocusable;
        public bool IsTextBoxFocusable
        {
            get { return _isFocusable; }
            set { _isFocusable = value; NotifyPropertyChanged(); }
        }

        private bool _isMapEnabled;
        public bool IsMapEnabled
        {
            get { return _isMapEnabled; }
            set { _isMapEnabled = value; NotifyPropertyChanged(); }
        }
        #endregion

        #region INotifyPropertyChanged implementation
        private bool canExecute = true;
        public event PropertyChangedEventHandler PropertyChanged;
        //public event EventHandler CanExecuteChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        #endregion
    }
}
