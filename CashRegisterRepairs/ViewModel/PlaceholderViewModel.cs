﻿using CashRegisterRepairs.Model;
using CashRegisterRepairs.Utilities.Data.MsWord;
using CashRegisterRepairs.Utilities.Locators;
using CashRegisterRepairs.View;
using CashRegisterRepairs.ViewModel.Common;
using CashRegisterRepairs.ViewModel.Common.Interfaces;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace CashRegisterRepairs.ViewModel
{
    public class PlaceholderViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<IViewModel> _tabViewModels;
        public ObservableCollection<IViewModel> TabViewModels { get { return _tabViewModels; } }

        public PlaceholderViewModel()
        {
            App.Current.MainWindow = ViewLocator.FindView<PlaceholderView>();

            // Initialize VMs of all TABS
            _tabViewModels = new ObservableCollection<IViewModel>();
            _tabViewModels.Add(new ClientsSitesViewModel(this));
            _tabViewModels.Add(new ModelsDevicesViewModel(this));
            _tabViewModels.Add(new TemplatesDocumentsViewModel());
            _tabViewModels.Add(new ServiceInfoViewModel());

            RemoveTempDocsCommand = new TemplateCommand(RemoveTempDocs, param => this.canExecuteCommand);
            CheckRequiredDocumentsCommand = new TemplateCommand(DetermineDevicesMissingRequiredDocument, param => this.canExecuteCommand);
        }

        private async void DetermineDevicesMissingRequiredDocument(object commandParameter)
        {
            MetroWindow placeholder = ViewLocator.GetMainView();
            List<string> devicesMissingRequiredDocument = new List<string>();

            DocumentWatchdog.DetermineRequiredDocuments();

            using (CashRegisterServiceContext dbModel = new CashRegisterServiceContext())
            {
                dbModel.Devices.ToList().ForEach(device =>
                {
                    if (!DocumentWatchdog.ValidateRequiredDocuments(device))
                    {
                        devicesMissingRequiredDocument.Add(device.DeviceModel.DEVICE_NUM_PREFIX + device.DEVICE_NUM_POSTFIX);
                    }
                });
            }

            if (devicesMissingRequiredDocument.Count != 0)
            {
                await placeholder.ShowMessageAsync("ПРЕДУПРЕЖДЕНИЕ", "Липсват задължителни документи за следните апарати: \n" + string.Join("\n", devicesMissingRequiredDocument));
            }
        }

        private void RemoveTempDocs(object commandParameter)
        {
            string[] tempDocs = Directory.GetFiles(ResourceLocator.temporaryDocumentsPath);

            if (tempDocs != null)
            {
                tempDocs.ToList().ForEach(tmp => File.Delete(tmp));
            }

            ViewLocator.FindAllViews().ForEach(v => v.Close());
        }

        private ICommand _removeTempDocsCommand;
        public ICommand RemoveTempDocsCommand
        {
            get { return _removeTempDocsCommand; }
            set { _removeTempDocsCommand = value; }
        }

        private ICommand _checkReqiredDocumentsCommand;
        public ICommand CheckRequiredDocumentsCommand
        {
            get { return _checkReqiredDocumentsCommand; }
            set { _checkReqiredDocumentsCommand = value; }
        }

        private int _selectedTab;
        public int SelectedTab
        {
            get { return _selectedTab; }
            set { _selectedTab = value; NotifyPropertyChanged(); }
        }

        private bool canExecuteCommand = true;
        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        #endregion
    }
}
