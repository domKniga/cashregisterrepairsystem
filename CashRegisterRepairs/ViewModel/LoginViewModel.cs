﻿using CashRegisterRepairs.Utilities.Locators;
using CashRegisterRepairs.Utilities.Security;
using CashRegisterRepairs.View;
using CashRegisterRepairs.ViewModel.Common;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Security;
using System.Windows.Input;

namespace CashRegisterRepairs.ViewModel
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        // FIELDS
        #region FIELDS
        private static readonly ILoginHandler loginHandler = new LoginHandler();
        private bool canExecuteCommand = true;
        #endregion

        public LoginViewModel()
        {
            LoginCommand = new TemplateCommand(TryLogin, param => this.canExecuteCommand);
        }

        //  METHODS
        private async void TryLogin(object commandParameter)
        {
            MetroWindow loginView = ViewLocator.FindView<LoginView>();

            if (string.IsNullOrEmpty(Username) || Password == null)
            {
                await loginView.ShowMessageAsync("ГРЕШКА", "Липсват данни!");
                return;
            }

            if (loginHandler.Login(Username, Password))
            {
                PlaceholderView homeView = new PlaceholderView();
                homeView.Show();

                loginView.Close();
            }
            else
            {
                await loginView.ShowMessageAsync("ГРЕШКА", "Грешни данни!");
            }

        }

        // COMMANDS
        private ICommand _loginCommand;
        public ICommand LoginCommand
        {
            get { return _loginCommand; }
            set { _loginCommand = value; }
        }

        // PROPERTIES
        #region PROPERTIES
        private string _username;
        public string Username
        {
            get { return _username; }
            set { _username = value; NotifyPropertyChanged(); }
        }

        private SecureString _password;
        public SecureString Password
        {
            get { return _password; }
            set { _password = value; NotifyPropertyChanged(); }
        }
        #endregion

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        #endregion
    }
}
